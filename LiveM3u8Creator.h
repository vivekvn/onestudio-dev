#include <bson.h>
#include <mongoc.h>
#include <stdio.h>
#include <ezxml.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <mysql.h>

#define MAX_FILES_IN_PLAYLIST 1000
#define FFMPEG_PATH "/home/vvek/DevTools/ffmpeg/ffmpeg"
#define CACHE_FAIL "CACHE_FAIL"
#define CACHE_DIRECTORY "cache"

//MYSQL
#define ID_FOR_VIDEO 0

//MANIFEST TEMPLATES
#define MANIFEST_HEADER "#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-TARGETDURATION:%d\n#EXT-X-MEDIA-SEQUENCE:%d\n"
#define MANIFEST_SEGMENT "#EXTINF:%.06f,%s/%s\n"

long getFileModificationTime(char *filePath);

typedef struct _config {
  char *playlistFile;
  char *customer;
  char *sessionId;
  char *m3u8Path;
} Config;

typedef struct _playlist{
  char *files[MAX_FILES_IN_PLAYLIST];
  float total_duration;
  float current_time;
  float buffer_level;
  long last_updated_time;
  int total_files;
}Playlist;

typedef struct _segmentList
{
    char *val;
    float duration;
    struct _segmentList *next;
}SegmentList;

typedef struct _status {
  int current_file;
  int current_line_in_file;
  SegmentList *segmentList;
  int media_sequence;
  char *files_cache[MAX_FILES_IN_PLAYLIST];
}Status;

static Config config;
static Playlist playlist;
static Status status;
static pthread_t m3u8Thread;
static pthread_t listSyncThread;
static pid_t ffmpegHLSProcessPid;
static MYSQL *mysql_con;
