#include "LiveM3u8Creator.h"

void init() {

  //Initialize Nulls for Globals
  config.playlistFile = NULL;
  config.customer = NULL;
  config.sessionId = NULL;
  config.m3u8Path = NULL;

  //Initialize playlist object
  playlist.files[0] = NULL;
  playlist.total_duration = 0.0;
  playlist.current_time = 0.0;
  playlist.buffer_level = 0.0;

  //Initialize Status
  status.current_file = -1;
  status.current_line_in_file = 4;
  status.files_cache[0] = CACHE_FAIL;
  status.segmentList = NULL;
  status.media_sequence = 0;

  //Mysql Init

  mysql_con = mysql_init(NULL);

  if (mysql_con == NULL)
  {
      printf("mysql_init() failed\n");
      exit(1);
  }

  if (mysql_real_connect(mysql_con, "localhost", "root", "9738000730", "onestudio", 0, NULL, 0) == NULL)
  {
    printf("Database connection error.");
    exit(1);
  }

}

char *getIdFromFilePath(char *filePath) {
  char query[256], *idForFile;
  sprintf(query, "SELECT * FROM videos where file_path='%s'", filePath);

  if (mysql_query(mysql_con, query))
  {
    printf("Query failed.\n");
  }

  MYSQL_RES *result = mysql_store_result(mysql_con);

  if (result == NULL)
  {
    printf("Fetching result from connection failed.\n");
  }

  int num_fields = mysql_num_fields(result);

  MYSQL_ROW row;

  while ((row = mysql_fetch_row(result)))
  {
    idForFile = row[ID_FOR_VIDEO];
    for(int i = 0; i < num_fields; i++)
    {
      printf("%s ", row[i] ? row[i] : "NULL");
    }
    printf("\n");
  }
  mysql_free_result(result);
  return idForFile;
}

void parse(char *line, char **argv)
{
    while (*line != '\0') {     /* if not the end of line ....... */
        while (*line == ' ' || *line == '\t' || *line == '\n')
            *line++ = '\0';     /* replace white spaces with 0    */
        *argv++ = line;          /* save the argument position     */
        while (*line != '\0' && *line != ' ' &&
               *line != '\t' && *line != '\n')
            line++;             /* skip the argument until ...    */
    }
    *argv = NULL;               /* mark the end of argument list  */
}


pid_t execute(char **argv, const char *dir, const char *outputFile)
{
    pid_t  pid, pid2;
    if ((pid = fork()) < 0) {     /* fork a child process           */
      printf("*** ERROR: forking child process failed\n");
      return -1;
    }
    else if (pid == 0) {          /* for the child process:         */
      chdir(dir);
      int fd;
      if (outputFile == NULL)
        fd = open("log.file", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
      else{
        fd = open(outputFile, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
      }
      //if(strstr(outputFile, "qPackager.log")!=NULL){
      dup2(fd, 1);
      dup2(fd, 2);
      //}
      close(fd);
      if (execvp(*argv, argv) < 0) {     /* execute the command  */
        printf("*** ERROR: exec failed\n");
        return -1;
      }
    }
    else {
      return pid;
    }
    return -1;
}

int parse_args(int argc, char *argv[]) {

  //Assign Configuration.
  printf("Streamer Configuration on Start: \n");
  config.sessionId = argv[1];
  printf("Session: %s\n", config.sessionId);
  if(argc>2)
  {
    config.playlistFile = argv[2];
    printf("Playlist: %s\n", config.playlistFile);
  }
  if(argc>3)
  {
    config.m3u8Path = argv[3];
    printf("M3u8Path: %s\n", config.m3u8Path);
  }
  if(argc>4)
  {
    config.customer = argv[4];
    printf("Customer: %s\n", config.customer);
  }
  return 0;

}

int parse_playlist() {
  ezxml_t playlistXML = ezxml_parse_file(config.playlistFile), files, file;
  if(playlistXML)
  {
    files = ezxml_child(playlistXML, "files");
    char *customer;
    int fileCounter = 0;
    ezxml_t path = NULL;

    customer = strdup(ezxml_attr(files, "customer"));
    config.customer = strdup(customer);

    printf("Parsing playlist files:\n");
    for (file = ezxml_child(files, "file"); file; file = file->next)
    {
      path = ezxml_child(file, "path");
      playlist.files[fileCounter++] = path?strdup(path->txt):NULL;
      printf("Playlist File @ %d is %s\n", (fileCounter-1), playlist.files[fileCounter-1]);
    }
    playlist.total_files=fileCounter;
    playlist.last_updated_time = getFileModificationTime(config.playlistFile);
    printf("The playlist was last modified at %ld\n", playlist.last_updated_time);
  }
  else {
    printf("Playlist file not found!\n");
    exit(1);
  }
  ezxml_free(playlistXML);
}

long getFileModificationTime(char *filePath)
{
  struct stat attrib;
  stat(filePath, &attrib);
  return attrib.st_ctime;
}

void createDirectory(char *path) {
  struct stat st = {0};
  if (stat(path, &st) == -1) {
      mkdir(path, 0777);
  }
}

void build_cache() {

  int filesCounter = status.current_file + 1;
  if(status.current_file > -1)
    filesCounter++; //Until duration based logic is handled.

  while (filesCounter < playlist.total_files )
  {
    char dir[128], *directory;
    directory = strdup(getIdFromFilePath(playlist.files[filesCounter]));
    status.files_cache[filesCounter] = directory;
    char  *argv[20];      /* the command line argument      */
    sprintf(dir, "./%s/%s/", CACHE_DIRECTORY, directory);
    createDirectory(dir);
    char ffmpegHLSProcess[512];
    sprintf(ffmpegHLSProcess, "%s -i %s -c copy -hls_time 4 -hls_list_size 0 master.m3u8", FFMPEG_PATH, playlist.files[filesCounter]);
    printf("FFMPEG HLS PROCESS: %s \n", ffmpegHLSProcess);
    parse(ffmpegHLSProcess, argv);       /*   parse the line               */
    ffmpegHLSProcessPid = execute(argv, dir, "process.log");
    int status;
    int result = waitpid(ffmpegHLSProcessPid,&status, 0);
    if(result == -1)
    {
      printf("Error in ffmpeg execution for %s.\n", playlist.files[filesCounter]);
    }
    filesCounter++;
  }
}

void *listSynchronize(void *threadid)
{
  printf("List SYNC started.\n");

  while(1) {

    if(getFileModificationTime(config.playlistFile) > playlist.last_updated_time)
    {
      sleep(5);
      parse_playlist();
      build_cache();
    }
    sleep(5);
  }

  pthread_exit(NULL);
}

char *getCurrentSegmentString() {
  char result[512];
  memset(result, 0, 512);
  char masterManifest[256], tsdirectory[256];
  sprintf(tsdirectory, "%s/%s/", CACHE_DIRECTORY,status.files_cache[status.current_file]);
  sprintf(masterManifest, "./%s/%s/master.m3u8", CACHE_DIRECTORY, status.files_cache[status.current_file]);

  printf("Getting segment from %s \n", masterManifest);

  FILE *file = fopen(masterManifest, "r");
  int count = 0;
  if ( file != NULL )
  {
      char line[256]; /* or other suitable maximum line size */
      while (fgets(line, sizeof line, file) != NULL) /* read a line */
      {
          if (count == status.current_line_in_file)
          {
              //use line or in a function return it
              //in case of a return first close the file with "fclose(file);"
              printf("Wanted: %s \n", line);
              if(count == 4)
                strcat(result, "#EXT-X-DISCONTINUITY\n");
              strcat(result, line);
              if (fgets(line, sizeof line, file) != NULL)
              {
                strcat(result, "\n");
                strcat(result,tsdirectory);
                strcat(result, line);
                strcat(result, "\n");
                break;
              }
              else
              {
                fclose(file);
                return NULL;
              }

          }
          else
          {
           count++;
          }
      }
      fclose(file);
      status.current_line_in_file += 2;
      return strdup(result);
  }
  else
  {
    exit(1);
  }
}

void *m3u8Creator(void *threadid)
{
  status.current_file = 0;
  while( status.current_file < playlist.total_files ) {
    printf("current file: %d, total_files: %d\n", status.current_file, playlist.total_files);

    char manifestFileString[4096], manifestHeader[256], *manifestSegments[5], loopCounter=5;
    memset(manifestFileString, 0, 4096);
    manifestSegments[0] = NULL;
    manifestSegments[1] = NULL;
    manifestSegments[2] = NULL;
    manifestSegments[3] = NULL;
    manifestSegments[4] = NULL;

    int segment_count = 0;
    SegmentList *newNode = (SegmentList *) malloc(sizeof(SegmentList));

    //Assign new node values;
    newNode->val = getCurrentSegmentString();
    if(newNode->val == NULL) {
      status.current_file++;
      status.current_line_in_file = 4;
      free(newNode);
      continue;
    }
    if(strstr(newNode->val,"DISCON") == NULL)
      newNode->duration = atof(newNode->val + 8);
    else
      newNode->duration = atof(newNode->val + 30);
    printf("Got segment %s duration: %.06f", newNode->val+30, newNode->duration);
    newNode->next = status.segmentList;
    status.segmentList = newNode;
    SegmentList *current_segment=status.segmentList;
    int maxDuration = 0;
    for(;loopCounter>-1;loopCounter--) {
      if (current_segment == NULL)
      {
        break;
      }
      manifestSegments[loopCounter-1] = strdup(current_segment->val);
      if(current_segment-> duration > maxDuration)
        maxDuration = current_segment->duration;
      if(loopCounter == 1 && current_segment->next != NULL)
      {
        free(current_segment->next);
        current_segment->next = NULL;
        status.media_sequence++;
      }
      current_segment = current_segment->next;
    }
    sprintf(manifestHeader, MANIFEST_HEADER, (int)maxDuration + 1, status.media_sequence);
    strcat(manifestFileString, manifestHeader);
    for(loopCounter = 0;loopCounter < 5; loopCounter++)
    {
      if(manifestSegments[loopCounter] != NULL)
      {
        strcat(manifestFileString, manifestSegments[loopCounter]);
      }
    }
    printf("Manifest File String: %s\n", manifestFileString );
    printf("sleeping for %f\n", status.segmentList->duration*1000000 - 100000);
    usleep(status.segmentList->duration*1000000 - 100000);
    //print last 5 segments

    FILE *f = fopen("master.m3u8", "w");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }
    fprintf(f, "%s", manifestFileString);
    fclose(f);
  }
}

int main(int argc, char *argv[])
{
  //Initialize variables
  int m3u8ThreadResult, listSyncThreadResult;
  init();
  printf("MySQL client version: %s\n", mysql_get_client_info());

  if(argc < 2){
      printf("Not enough arguments.\n");
      return 1;
  }

  parse_args(argc, argv);
  parse_playlist();
  build_cache();

  m3u8ThreadResult = pthread_create(&m3u8Thread, NULL, m3u8Creator, (void *)NULL);
  listSyncThreadResult = pthread_create(&listSyncThread, NULL, listSynchronize, (void *) NULL);
  if (m3u8ThreadResult){
    printf("ERROR; return code from pthread_create() is %d\n", m3u8ThreadResult);
    exit(-1);
  }
  pthread_join(m3u8Thread, NULL);

  return 0;

}
