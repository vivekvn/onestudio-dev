# Author: Vivek N

CC=gcc -g
CFLAGS=-I./libs/ezxml/ -I/usr/include/libbson-1.0/ -I/usr/include/libmongoc-1.0/ -I/usr/include/mysql/ -I./
LFLAGS=-L./libs/ezxml/ -L/usr/lib64/mysql/
LIBS=-lezxml -lmongoc-1.0 -lmysqlclient -lpthread

default: LiveM3u8Creator.c
	$(CC) $(CFLAGS) $(LFLAGS) LiveM3u8Creator.c $(LIBS) -o LiveM3u8Creator